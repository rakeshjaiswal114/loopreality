import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AssessmentsComponent } from './assessments/assessments.component';
import { ModulesComponent } from './modules/modules.component';
import { PeopleComponent } from './people/people.component';
import { SettingsComponent } from './settings/settings.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainComponent } from './main/main.component';
import { DashboardService } from './shared/dashboard.service';
const appRoutes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  // { path: 'dashboard', component: DashboardComponent },
  {
    path: '',
    component: MainComponent,
    children: [

      { path: 'dashboard', component: DashboardComponent },
      { path: 'assessments', component: AssessmentsComponent },
      { path: 'people', component: PeopleComponent },
      { path: 'modules', component: ModulesComponent },
      { path: 'settings', component: SettingsComponent },
    ],

  },
  { path: '**', component: DashboardComponent }

];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AssessmentsComponent,
    ModulesComponent,
    PeopleComponent,
    SettingsComponent,
    SidebarComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
    ),HttpClientModule
  ],
  providers: [DashboardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
