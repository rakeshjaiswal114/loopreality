import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }
  getStrength() {
    return this.http.get('assets/strength.json');
  }
  getBenchmark() {
    return this.http.get('assets/benchmark.json');
  }
  getUsers() {
    return this.http.get('assets/user.json');
  }
 
}
