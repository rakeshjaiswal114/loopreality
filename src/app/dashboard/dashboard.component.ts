import { Component, OnInit } from '@angular/core';
import { DashboardService } from './../shared/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  strengthList:string[]; 
  benchmarkList:string[];
  userList:string[];
  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.strengthList = [];
    this.benchmarkList = [];
    this.userList = [];
    this.getStrength();
    this.getBenchmark();
    this.getUsers();
  }

  getUsers() {
    try {
      this.dashboardService.getUsers()
        .subscribe(response => {
          this.userList = response
        }, error => {
          console.log('==Get error Details==', error)
        })
    } catch (ex) {
      console.log(ex)
    }
  }

  getStrength() {
    try {
      this.dashboardService.getStrength()
        .subscribe(response => {
          this.strengthList = response
        }, error => {
          console.log('==Get error Details==', error)
        })
    } catch (ex) {
      console.log(ex)
    }
  }

  getBenchmark() {
    try {
      this.dashboardService.getBenchmark()
        .subscribe(response => {
          this.benchmarkList = response
        }, error => {
          console.log('==Get error Details==', error)
        })
    } catch (ex) {
      console.log(ex)
    }
  }

}
